#!/bin/bash

function is_partition_space_low(){
    PARTITION=$1
    THRESHOLD=$2

    CURRENT=$(df -P $PARTITION | grep $PARTITION | awk '{ print $5}'| sed 's/%//g')
    EMAILTO='rotts@uwf.edu'
    HOST=$(hostname)
    if [ "$CURRENT" -gt "$THRESHOLD" ] ; then
        mail -s "DiskAlert: $(hostname) Disk Space Alert" $EMAILTO << EOF
    Your $PARTITION partition remaining free space is critically low. Used: $CURRENT%
EOF
    fi

}

is_partition_space_low / 75
is_partition_space_low '/opt' 70
is_partition_space_low '/boot' 70